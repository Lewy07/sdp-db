package com.example.SDP;

import com.example.SDP.models.Book;
import com.example.SDP.repositories.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.util.List;


@SpringBootApplication
public class SdpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdpApplication.class, args);
	}
	@Bean
	public CommandLineRunner demo(BookRepository repository) {
		return (args) -> {

			Book firstBook = new Book();
			Book secondBook = new Book();
			Book thirdBook = new Book();

			firstBook.setName("Rok 1984");
			secondBook.setName("Lalka");
			thirdBook.setName("Ksiazka");

			//Insert
			repository.save(firstBook);
			repository.save(secondBook);
			repository.save(thirdBook);

			//Select *
			List<Book> libary = repository.findAll();
			System.out.println("\n -- SHOW ALL -- ");
			libary.forEach(System.out::println);

			//Select one by Id
			System.out.println("\n -- SHOW ONE -- ");
			System.out.println(repository.findById(1L));

			//Delate one by Id
			System.out.println("\n -- DELETE ONE -- ");
			repository.deleteById(1L);

			//Select *
			System.out.println("\n -- SHOW ALL -- ");
			libary = repository.findAll();
			libary.forEach(System.out::println);
		};
	}
}