package com.example.SDP.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;


@Entity
public class Book {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    @Getter
    private Long id;
    @Setter
    @Getter
    private String name;

    @Override
    public String toString() {return "Book title: " + this.name;}
}