package com.example.SDP.services;


import com.example.SDP.models.Book;
import com.example.SDP.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;


    public List<Book> list(){
        return bookRepository.findAll();
    }
}
